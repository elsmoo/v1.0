Welcome in the ElsMoo v1.0 Amino Acids program!

To start this program there are some requirements.
1. Have a postgresql database called aminoacids
2. Have a user called 'ElsMoo' with password 'ElsMoo'
3. Load the dumps/dump.sql file into the aminoacids database
4. Open IntelliJ IDEA and load the postgresql-42.2.2.jar file as an external library, this is the driver
5. Run ElsMooController
6. Enjoy.