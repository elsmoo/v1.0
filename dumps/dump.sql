--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: properties; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.properties (
    fullname character varying(13) NOT NULL,
    onelettercode character(1) NOT NULL,
    threelettercode character(3) NOT NULL,
    hydro character varying(12) NOT NULL,
    charge character varying(8) NOT NULL,
    size character varying(6) NOT NULL,
    pref character varying(6) NOT NULL,
    structure character(1)
);


ALTER TABLE public.properties OWNER TO "ElsMoo";

--
-- Name: questions; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.questions (
    fill integer NOT NULL,
    question character varying(255) NOT NULL,
    answerid integer,
    questionid integer
);


ALTER TABLE public.questions OWNER TO "ElsMoo";

--
-- Name: typeof; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.typeof (
    questionid integer NOT NULL,
    typeofquestion character varying(52) NOT NULL
);


ALTER TABLE public.typeof OWNER TO "ElsMoo";

--
-- Data for Name: properties; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.properties (fullname, onelettercode, threelettercode, hydro, charge, size, pref, structure) FROM stdin;
Cysteine	C	Cys	Moderate	Neutral	Medium	Strand	C
Asparagine	N	Asn	Hydrophilic	Neutral	Medium	Turn	N
Phenylalanine	F	Phe	Hydrophobic	Neutral	Big	Strand	F
Glycine	G	Gly	Moderate	Neutral	Small	Turn	G
Threonine	T	Thr	Moderate	Neutral	Small	Strand	T
Aspartic acid	D	Asp	Hydrophilic	Negative	Medium	Helix	D
Methionine	M	Met	Hydrophobic	Neutral	Big	Helix	M
Isoleucine	I	Ile	Hydrophobic	Neutral	Medium	Strand	I
Glutamine	Q	Gln	Hydrophilic	Neutral	Big	None	Q
Serine	S	Ser	Moderate	Neutral	Small	Turn	S
Tryptophan	W	Trp	Hydrophobic	Neutral	Big	Strand	W
Arginine	R	Arg	Hydrophilic	Positive	Big	None	R
Histidine	H	His	Hydrophilic	Positive	Big	None	H
Leucine	L	Leu	Hydrophobic	Neutral	Medium	Helix	L
Valine	V	Val	Hydrophobic	Neutral	Small	Strand	V
Glutamic acid	E	Glu	Hydrophilic	Negative	Big	Helix	E
Alanine	A	Ala	Hydrophobic	Neutral	Small	Helix	A
Proline	P	Pro	Hydrophobic	Neutral	Small	Turn	P
Tyrosine	Y	Tyr	Hydrophobic	Neutral	Big	Strand	Y
Lysine	K	Lys	Hydrophilic	Positive	Big	Helix	K
\.


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.questions (fill, question, answerid, questionid) FROM stdin;
3	What is the hydrophobicity of amino acid 	4	3
2	What is the hydrofobicity of amino acid 	4	2
3	What is the hydrofobicity of amino acid 	4	4
1	What is the hydrofobicity of amino acid 	4	4
2	What is the hydrofobicity of amino acid 	4	4
4	What amino acid is 	2	4
4	What amino acid is not 	2	4
4	What amino acid is 	3	4
4	What amino acid is not 	3	4
5	What amino acid is 	2	5
5	What amino acid is not 	2	5
6	What amino acid is 	2	6
6	What amino acid is not 	2	6
5	What amino acid is 	3	5
5	What amino acid is not 	3	5
6	What amino acid is 	3	6
6	What amino acid is not 	3	6
7	What amino acid has a preferation for a 	2	7
7	What amino acid has no preferation for a 	2	7
7	What amino acid has a preferation for a 	3	7
7	What amino acid has no preferation for a 	3	7
3	What is the 3D preferation of amino acid 	7	7
5	What amino acid is not 	1	5
5	What amino acid is 	1	5
4	What amino acid is not 	1	4
1	What is the size of 	6	1
2	What is the 3D preferation of amino acid 	7	2
3	What is the 1-lettercode of amino acid 	2	3
3	What is the charge of amino acid 	5	5
2	What is the charge of amino acid 	5	5
1	What is the sidechain of 	8	1
3	What is the charge of amino acid 	5	3
3	What is the size of amino acid 	6	3
2	What is the size of amino acid 	6	2
2	What is the sidechain of 	8	2
3	What is the sidechain of 	8	3
8	What is the hydrophobicity of the amino acid with the following sidechain 	4	4
8	What is the charge of the amino acid with the following sidechain 	5	5
8	What is the size of the amino acid with the following sidechain 	6	6
8	What is the 3D preferation of the amino acid with the following sidechain 	7	7
8	What is the full name of the amino acid with the following sidechain 	1	8
8	What is the 1-lettercode of the amino acid with the following sidechain 	2	8
8	What is the hydrophobicity of the amino acid with the following sidechain 	4	8
1	What is the 3-lettercode of 	3	1
6	What amino acid is not 	1	6
3	What is the full name of amino acid 	1	3
1	What is the 3D preferation of amino acid 	7	7
1	What is the 1-lettercode of 	2	1
1	What is the charge of amino acid 	5	5
1	What is the hydrofobicity of 	4	1
2	What is the size of amino acid 	6	6
2	What is the full name of amino acid 	1	2
2	What is the 3D preferation of amino acid 	7	7
3	What is the size of amino acid 	6	6
1	What is the 3D preferation of 	7	1
7	What amino acid has a preferation for a 	1	7
3	What is the 3D preferation of amino acid 	7	3
4	What amino acid is 	1	4
2	What is the charge of amino acid 	5	2
6	What amino acid is 	1	6
1	What is the size of amino acid 	6	6
2	What is the 1-lettercode of amino acid 	3	2
7	What amino acid has no preferation for a 	1	7
1	What is the charge of 	5	1
8	What is the charge of the amino acid with the following sidechain 	5	8
8	What is the size of the amino acid with the following sidechain 	6	8
8	What is the 3D preferation of the amino acid with the following sidechain 	7	8
\.


--
-- Data for Name: typeof; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.typeof (questionid, typeofquestion) FROM stdin;
2	onelettercode
3	threelettercode
4	hydro
5	charge
6	size
1	fullname
7	pref
8	structure
\.


--
-- Name: properties properties_onelettercode_key; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_onelettercode_key UNIQUE (onelettercode);


--
-- Name: properties properties_pkey; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (fullname);


--
-- Name: properties properties_threelettercode_key; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_threelettercode_key UNIQUE (threelettercode);


--
-- Name: typeof questions_pkey; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.typeof
    ADD CONSTRAINT questions_pkey PRIMARY KEY (questionid);


--
-- Name: questions_questionid_uindex; Type: INDEX; Schema: public; Owner: ElsMoo
--

CREATE UNIQUE INDEX questions_questionid_uindex ON public.typeof USING btree (questionid);


--
-- Name: questions questions_typeofquestions_01; Type: FK CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_typeofquestions_01 FOREIGN KEY (fill) REFERENCES public.typeof(questionid);


--
-- Name: questions questions_typeofquestions_02; Type: FK CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_typeofquestions_02 FOREIGN KEY (answerid) REFERENCES public.typeof(questionid);


--
-- PostgreSQL database dump complete
--

