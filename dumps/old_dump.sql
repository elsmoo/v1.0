--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: properties; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.properties (
    fullname character varying(13) NOT NULL,
    onelettercode character(1) NOT NULL,
    threelettercode character(3) NOT NULL,
    hydro character varying(12) NOT NULL,
    charge character varying(8) NOT NULL,
    size character varying(6) NOT NULL,
    pref character varying(6) NOT NULL,
    chain character(1)
);


ALTER TABLE public.properties OWNER TO "ElsMoo";

--
-- Name: questions; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.questions (
    fill integer NOT NULL,
    question character varying(255) NOT NULL,
    answerid integer,
    questionid integer
);


ALTER TABLE public.questions OWNER TO "ElsMoo";

--
-- Name: typeof; Type: TABLE; Schema: public; Owner: ElsMoo
--

CREATE TABLE public.typeof (
    questionid integer NOT NULL,
    typeofquestion character varying(52) NOT NULL
);


ALTER TABLE public.typeof OWNER TO "ElsMoo";

--
-- Data for Name: properties; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.properties (fullname, onelettercode, threelettercode, hydro, charge, size, pref, chain) FROM stdin;
Glutaminezuur	E	Glu	Hydrofiel	Negatief	Groot	Helix	\N
Aspartaatzuur	D	Asp	Hydrofiel	Negatief	Middel	Helix	\N
Proline	P	Pro	Hydrofoob	Neutraal	Klein	Turn	\N
Tyrosine	Y	Tyr	Hydrofoob	Neutraal	Groot	Strand	\N
Glycine	G	Gly	Neutraal	Neutraal	Klein	Turn	\N
Valine	V	Val	Hydrofoob	Neutraal	Klein	Strand	\N
Tryptophan	W	Trp	Hydrofoob	Neutraal	Groot	Strand	\N
Phenylalanine	F	Phe	Hydrofoob	Neutraal	Groot	Strand	\N
Isoleucine	I	Ile	Hydrofoob	Neutraal	Middel	Strand	\N
Leucine	L	Leu	Hydrofoob	Neutraal	Middel	Helix	\N
Cysteine	C	Cys	Neutraal	Neutraal	Middel	Strand	\N
Glutamine	Q	Gln	Hydrofiel	Neutraal	Groot	Geen	\N
Threonine	T	Thr	Neutraal	Neutraal	Klein	Strand	\N
Serine	S	Ser	Neutraal	Neutraal	Klein	Turn	\N
Alanine	A	Ala	Hydrofoob	Neutraal	Klein	Helix	\N
Asparagine	N	Asn	Hydrofiel	Neutraal	Middel	Turn	\N
Methionine	M	Met	Hydrofoob	Neutraal	Groot	Helix	\N
Arginine	R	Arg	Hydrofiel	Positief	Groot	Geen	\N
Histidine	H	His	Hydrofiel	Positief	Groot	Geen	\N
Lysine	K	Lys	Hydrofiel	Positief	Groot	Helix	\N
\.


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.questions (fill, question, answerid, questionid) FROM stdin;
1	Wat is de 3D voorkeur van aminozuur 	7	7
1	Wat is de grootte van aminozuur 	6	6
2	Wat is de grootte van aminozuur 	6	6
2	Wat is de grootte van aminozuur 	6	2
3	Wat is de grootte van aminozuur 	6	6
3	Wat is de 3D voorkeur van aminozuur 	7	3
2	Wat is de 3D voorkeur van aminozuur 	7	2
3	Wat is de volledige naam van aminozuur 	1	3
2	Wat is de volledige naam van aminozuur 	1	2
3	Wat is de lading van aminozuur 	5	3
4	Welk aminozuur is niet 	1	4
4	Welk aminozuur is 	1	4
2	Wat is de 3D voorkeur van aminozuur 	7	7
3	Wat is de 3D voorkeur van aminozuur 	7	7
2	Wat is de lading van aminozuur 	5	5
2	Wat is de hydrofobiteit van aminozuur 	4	4
3	Wat is de hydrofobiteit van aminozuur 	4	4
1	Wat is de lading van aminozuur 	5	5
1	Wat is de hydrofobiteit van aminozuur 	4	4
3	Wat is de lading van aminozuur 	5	5
3	Wat is de hydrofobiciteit van aminozuur 	4	3
3	Wat is de grootte van aminozuur 	6	3
7	Welk aminozuur heeft geen voorkeur voor een 	1	7
1	Wat is de grootte van 	6	1
1	Wat is de lading van 	5	1
3	Wat is de 1-lettercode van aminozuur 	2	3
2	Wat is de hydrofobiciteit van aminozuur 	4	2
2	Wat is de lading van aminozuur 	5	2
2	Wat is de 3-lettercode van aminozuur 	3	2
1	Wat is de 3D voorkeur van 	7	1
1	Wat is de 3-lettercode van 	3	1
1	Wat is de 1-lettercode van 	2	1
1	Wat is de hydrofobiciteit van 	4	1
6	Welk aminozuur is 	1	6
7	Welk aminozuur heeft een voorkeur voor 	1	7
5	Welk aminozuur is niet 	1	5
5	Welk aminozuur is 	1	5
6	Welk aminozuur is niet 	1	6
\.


--
-- Data for Name: typeof; Type: TABLE DATA; Schema: public; Owner: ElsMoo
--

COPY public.typeof (questionid, typeofquestion) FROM stdin;
2	onelettercode
3	threelettercode
4	hydro
5	charge
6	size
8	chain
1	fullname
7	pref
\.


--
-- Name: properties properties_onelettercode_key; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_onelettercode_key UNIQUE (onelettercode);


--
-- Name: properties properties_pkey; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (fullname);


--
-- Name: properties properties_threelettercode_key; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_threelettercode_key UNIQUE (threelettercode);


--
-- Name: typeof questions_pkey; Type: CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.typeof
    ADD CONSTRAINT questions_pkey PRIMARY KEY (questionid);


--
-- Name: questions_questionid_uindex; Type: INDEX; Schema: public; Owner: ElsMoo
--

CREATE UNIQUE INDEX questions_questionid_uindex ON public.typeof USING btree (questionid);


--
-- Name: questions questions_typeofquestions_01; Type: FK CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_typeofquestions_01 FOREIGN KEY (fill) REFERENCES public.typeof(questionid);


--
-- Name: questions questions_typeofquestions_02; Type: FK CONSTRAINT; Schema: public; Owner: ElsMoo
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_typeofquestions_02 FOREIGN KEY (answerid) REFERENCES public.typeof(questionid);


--
-- PostgreSQL database dump complete
--

