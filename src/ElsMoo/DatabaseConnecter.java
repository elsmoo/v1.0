package ElsMoo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

class DatabaseConnecter {
    private Connection myConnection;

    DatabaseConnecter() {
        try {
            Class.forName("org.postgresql.Driver").newInstance();
            String dbUrl = "jdbc:postgresql://localhost:5432/aminoacids";
            String username = "ElsMoo";
            String password = "ElsMoo";

            //get a connection
            myConnection = DriverManager.getConnection(dbUrl, username, password);
        } catch (Exception ex) {
            // handle the error
            System.out.println(ex.getMessage());
        }
    }

    //Should be a better alternative, ask @Miggel
    ArrayList<String> getQuestion() {
        ArrayList<String> arrayList = new ArrayList<>();
        //Pick question
        String question = "";
        String typeOfQuestion = "";
        String typeOfAnswerFull = "";
        String typeOfFill = "";
        do {
            try {
                //create a statement
                Statement myStatement = myConnection.createStatement();
                //execute statement
                ResultSet myResultSet = myStatement.executeQuery(
                        "SELECT question, qid.typeofquestion AS typeofquestionfull, aid.typeofquestion AS typeofanswerfull, fi.typeofquestion AS fill FROM questions INNER JOIN typeof qid ON questions.questionid = qid.questionid INNER JOIN typeof aid ON questions.answerid = aid.questionid INNER JOIN typeof fi ON questions.questionid = fi.questionid WHERE qid.typeofquestion != 'chain' AND aid.typeofquestion != 'chain' ORDER BY RANDOM();");
                while (myResultSet.next()) {
                    question = myResultSet.getString("question");
                    typeOfAnswerFull = myResultSet.getString("typeofanswerfull");
                    typeOfQuestion = myResultSet.getString("typeofquestionfull");
                    typeOfFill = myResultSet.getString("fill");
                    break;
                }
            } catch (Exception ex) {
                // handle the error
                System.out.println(ex.getMessage());
            }
        } while (typeOfAnswerFull.equals(typeOfFill));
        return fetchAnswers(arrayList, question, typeOfQuestion, typeOfFill, typeOfAnswerFull);
    }

    ArrayList<String> getQuestion(ArrayList<String> typeOfQuestions, ArrayList<String> typeOfAnswers) {
        ArrayList<String> arrayList = new ArrayList<>();
        //Pick question
        String question = "";
        String typeOfQuestion = "";
        String typeOfAnswerFull = "";
        String typeOfFill = "";
        do {
            try {
                //create a statement
                Statement myStatement = myConnection.createStatement();
                //execute statement
                ResultSet myResultSet = myStatement.executeQuery(
                        "SELECT question, qid.typeofquestion AS typeofquestionfull, " +
                                "aid.typeofquestion AS typeofanswerfull, " +
                                "fi.typeofquestion AS filling " +
                                "FROM questions " +
                                "INNER JOIN typeof qid ON questions.questionid = qid.questionid " +
                                "INNER JOIN typeof aid ON questions.answerid = aid.questionid " +
                                "INNER JOIN typeof fi ON questions.questionid = fi.questionid " +
                                "WHERE qid.typeofquestion IN (" + ArrayListToString(typeOfQuestions).substring(0, ArrayListToString(typeOfQuestions).length() - 2) + ") " +
                                "AND aid.typeofquestion IN (" + ArrayListToString(typeOfAnswers).substring(0, ArrayListToString(typeOfAnswers).length() - 2) + ") " +
                                "ORDER BY RANDOM();");
                while (myResultSet.next()) {
                    question = myResultSet.getString("question");
                    typeOfAnswerFull = myResultSet.getString("typeofanswerfull");
                    typeOfQuestion = myResultSet.getString("typeofquestionfull");
                    typeOfFill = myResultSet.getString("filling");
                    break;
                }
            } catch (Exception ex) {
                // handle the error
                System.out.println(ex.getMessage());
            }
        } while (typeOfAnswerFull.equals(typeOfFill));

        return fetchAnswers(arrayList, question, typeOfQuestion, typeOfFill, typeOfAnswerFull);
    }

    private ArrayList<String> fetchAnswers(ArrayList<String> arrayList, String question, String typeOfQuestion, String typeOfFill, String typeOfAnswerFull){
        ArrayList<String> answers = new ArrayList<>();
        String acid = "";
        String acidFill = "";
        try {
            //create a statement
            Statement myStatement = myConnection.createStatement();
            //execute statement
            ResultSet myResultSet = myStatement.executeQuery(
                    "SELECT * FROM properties ORDER BY RANDOM() LIMIT 1;");
            while (myResultSet.next()) {
                acid = myResultSet.getString(typeOfQuestion);
                acidFill = myResultSet.getString(typeOfFill);
                answers.add(myResultSet.getString(typeOfAnswerFull));
                break;
            }
        } catch (Exception ex) {
            // handle the error
            System.out.println(ex.getMessage());
        }
        arrayList.add(question + ((acidFill.length() < 4) ? acidFill : acidFill.toLowerCase()) + "?");
        //Fetch wrong answers
        String answer = "";
        try {
            //create a statement
            Statement myStatement = myConnection.createStatement();
            //execute statement
            ResultSet myResultSet = myStatement.executeQuery(
                    "SELECT DISTINCT(" + typeOfAnswerFull + ") FROM properties WHERE " + typeOfAnswerFull + " != '" + acid + "'  AND " + typeOfQuestion + " != '" + acid + "' AND " + typeOfAnswerFull + " != '" + answers.get(0) + "' LIMIT " + ((!typeOfAnswerFull.equals("pref")) ? 2 : 3) + ";");
            while (myResultSet.next()) {
                answer = myResultSet.getString(typeOfAnswerFull);
                answers.add(answer);
            }
        } catch (Exception ex) {
            // handle the error
            System.out.println(ex.getMessage());
        }
        arrayList.add(answers.get(0));
        Collections.shuffle(answers);
        arrayList.addAll(answers);
        return arrayList;
    }

    private String ArrayListToString(ArrayList<String> arrayList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String type : arrayList) {
            stringBuilder.append("'");
            stringBuilder.append(type);
            stringBuilder.append("', ");
        }
        return stringBuilder.toString();
    }

}