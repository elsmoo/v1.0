package ElsMoo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.PrintWriter;

public class ElsMooController extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        settingsReader.resetSettings();
//        PrintWriter writer = new PrintWriter("settings.txt", "UTF-8");
//        writer.println("name=Said");
//        writer.println("numberOfQuestions=50");
//        writer.println("questionFullName=false");
//        writer.println("questionOneLetterCode=true");
//        writer.println("questionThreeLetterCode=false");
//        writer.println("questionHydro=false");
//        writer.println("questionCharge=false");
//        writer.println("questionSize=false" );
//        writer.println("questionpref=false");
//        writer.println("questionStructure=false");
//        writer.println("answerFullName=true");
//        writer.println("answerOneLetterCode=false");
//        writer.println("answerThreeLetterCode=false");
//        writer.println("answerHydro=false");
//        writer.println("answerCharge=false");
//        writer.println("answerSize=false");
//        writer.println("answerpref=false");
//        writer.println("answerStructure=false");
//        writer.println("timeOn=true");
//        writer.println("time=5");
//        writer.println("fromOptionsScreen=false");
//        writer.close();
        Parent root = FXMLLoader.load(getClass().getResource("homeScreen.fxml"));

        Scene scene = new Scene(root, 500, 600);

        stage.setTitle("FXML Welcome");
        stage.setScene(scene);
        stage.show();
    }
}
