package ElsMoo;



import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.awt.*;
import java.io.*;
import java.security.cert.Extension;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import static javafx.scene.media.MediaPlayer.INDEFINITE;


public class ExamScreenController {
    @FXML private Text title;
    @FXML private Text errorText;
    @FXML private Text fileContainment;
    @FXML private Label numberOfQuestionsLabel;
    @FXML private ComboBox<Integer> numberOfQuestions;
    @FXML private Desktop desktop = Desktop.getDesktop();
    @FXML private javafx.scene.control.Button examsButton;
    @FXML private Text actionStatus;
    @FXML private Text done;
    @FXML private javafx.scene.control.Button fileChooserButton;
    @FXML private javafx.scene.control.Button backButton;
    @FXML private javafx.scene.control.Button exitButton;
    private AudioClip shutDown = new AudioClip(getClass().getResource("music/shutDown.wav").toExternalForm());


    @FXML protected void initialize() {
        //Label of number of questions
        numberOfQuestionsLabel.setText("Number of questions:");
        //Fill combobox and set default choice
        numberOfQuestions.getItems().addAll(30, 40, 50, 60, 70);
        numberOfQuestions.setValue(50);
    }

    @FXML
    protected void chooseFileButtonAction(){showSingleFileChooser();}

    @FXML
    protected void makeExamsActionButton(){
        if (fileContainment.getText().equals("")){
            setError("Error with this file, please select a different one");
            return;
        }
        else { //Need to refactor this
            setError("");
        }
        DatabaseConnecter databaseConnecter = new DatabaseConnecter();
        //Function for making exams not done yet...
        for (int student=0; student < fileContainment.getText().split("\n").length; student++) {
            ArrayList<String> currentQuestion = new ArrayList<>();
            try {
                if (!fileContainment.getText().split("\n")[student].split(" ")[0].equals("")) {
                    PrintWriter questionWriter = new PrintWriter("AAtest_" + fileContainment.getText().split("\n")[student].split(" ")[0] + "_" + getTime() + ".txt", "UTF-8");
                    PrintWriter answerWriter = new PrintWriter("AAant_" + fileContainment.getText().split("\n")[student].split(" ")[0] + "_" + getTime() + ".txt", "UTF-8");
                    for (byte i = 1; i <= numberOfQuestions.getValue(); i++) {
                        currentQuestion = databaseConnecter.getQuestion();
                        questionWriter.println("Vraag " + i + ".");
                        questionWriter.println(currentQuestion.get(0));
                        questionWriter.println(currentQuestion.get(2));
                        questionWriter.println(currentQuestion.get(3));
                        questionWriter.println(currentQuestion.get(4));
                        questionWriter.println("\n");

                        answerWriter.println("Vraag " + i + ".");
                        answerWriter.println(currentQuestion.get(0));
                        answerWriter.println(currentQuestion.get(1));
                        questionWriter.println("\n");

                    }
                    questionWriter.close();
                    answerWriter.close();
                    done.setText("Exams created!");
                }
            } catch (FileNotFoundException | UnsupportedEncodingException e) {
                setError("Error with writing the files");
            } catch (ArrayIndexOutOfBoundsException e){
                setError("No names given.. No exams created");
            }
        }
    }

    @FXML
    protected void backButtonAction(){ changeScene("homeScreen");}

    @FXML
    protected void exitButtonAction(){
        final Task task = new Task() {

            @Override
            protected Object call() {
                shutDown.setVolume(0.5f);
                shutDown.setCycleCount(INDEFINITE);
                shutDown.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        try {
            Thread.sleep(1750);}
        catch (InterruptedException ie){
            System.out.println("Couldn´t sleep...");
        }
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @FXML private void changeScene(String scene) {
        try {
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) title.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length()-6) + " screen");
        } catch (IOException IOE){
            System.out.println("Whoops, couldn't load the new scene...");
        }
    }

    private void showSingleFileChooser() {
        fileContainment.setText("");
        StringBuilder stringBuilder = getWordsFromFile();
        String string[] = stringBuilder.toString().split("\n");
        for (int count = 0; count <= 9; count ++){
            fileContainment.setText(fileContainment.getText()+"\n"+string[count]);
        }
    }

    private StringBuilder getWordsFromFile(){
        //Let´s try open the file and see what we get.
        StringBuilder stringBuilder = new StringBuilder();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter(".txt",
                "*.txt", "*.text");
        fileChooser.getExtensionFilters().add(filter);
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            try {
                Scanner scanner = new Scanner(new File(selectedFile.getName()));
                while (scanner.hasNextLine()) {
                    stringBuilder.append(scanner.nextLine().split(" ")[0]);
                    stringBuilder.append("\n");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        // Catching When selectedfile is equal to Null
        if (selectedFile == null){
            setError("Error with loading file. Did you choose a file?");
            for (int count=10;count >= 0; count --){
                stringBuilder.append(" \n");
            }
        }
        return stringBuilder;
    }

    private void setError(String error){
        errorText.setText(error);
    }

    private static String getTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yy:HH:mm:ss");
        return sdf.format(cal.getTime());
    }
}
