package ElsMoo;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

class Fraction extends VBox {
    private double offset;

    public Fraction(int numerator, int denominator) {
        init(numerator + "", denominator + "");
    }

    public Fraction(String numerator, String denominator) {
        init(numerator, denominator);
    }

    private void init(String numerator, String denominator) {
        setAlignment(Pos.CENTER);

        Text numeratorText   = new Text(numerator);
        Text denominatorText = new Text(denominator);

        offset = numeratorText.getBaselineOffset() * 1.5;

        double dividerWidth =
                Math.max(
                        numeratorText.getLayoutBounds().getWidth(),
                        denominatorText.getLayoutBounds().getWidth()
                ) + 6;

        Line divider = new Line(0, 1, dividerWidth, 1);
        divider.setStrokeWidth(2);

        getChildren().addAll(
                numeratorText,
                divider,
                denominatorText
        );
    }

    public double getBaselineOffset() {
        return offset;
    }
}
