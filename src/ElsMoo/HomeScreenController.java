package ElsMoo;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.IOException;

import static javafx.scene.media.MediaPlayer.INDEFINITE;

public class HomeScreenController {
    @FXML private Text title;
    @FXML private Text description;
    @FXML private javafx.scene.control.Button startButton;
    @FXML private javafx.scene.control.Button optionsButton;
    @FXML private javafx.scene.control.Button makeExamsButton;
    @FXML private javafx.scene.control.Button exitButton;
    private AudioClip audio = new AudioClip(getClass().getResource("music/lobby.wav").toExternalForm());
    private AudioClip shutDown = new AudioClip(getClass().getResource("music/shutDown.wav").toExternalForm());


    @FXML protected void initialize() {
        description.setText("To make exams, press the 'Make exams' button.\nTo take an exam, press the 'Take Exam' button. \nIf you want to change the options first, press the 'Options' button first. \nTo exit, press 'Exit'");
        final Task task = new Task() {

            @Override
            protected Object call() {
                audio.setVolume(0.5f);
                audio.setCycleCount(INDEFINITE);
                audio.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }

    @FXML protected void startButtonAction() {
        //Need to add errorCode
        if (settingsReader.getName().equals("") || settingsReader.getSettings(false).size() == 0 ||
                settingsReader.getSettings(true).size() == 0){
            changeScene("optionsScreen");
        } else {
            changeScene("playScreen");
        }
    }

    @FXML protected void makeExamsButtonAction() { changeScene("examScreen");}

    @FXML protected void optionsButtonAction() {
        changeScene("optionsScreen");
    }

    @FXML
    protected void exitButtonAction(){
        audio.stop();
        final Task task = new Task() {

            @Override
            protected Object call() {
                shutDown.setVolume(0.5f);
                shutDown.setCycleCount(INDEFINITE);
                shutDown.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        try {
        Thread.sleep(1750);}
        catch (InterruptedException ie){
            System.out.println("Couldn´t sleep...");
        }
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @FXML private void changeScene(String scene) {
        try {
            audio.stop();
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) title.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length()-6) + " screen");
        } catch (IOException IOE){
            System.out.println("Whoops, couldn't load the new scene...");
        }
    }
}
