package ElsMoo;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.IOException;

import static javafx.scene.media.MediaPlayer.INDEFINITE;

public class OptionsScreenController {
    @FXML private ToolBar header;
    @FXML private javafx.scene.control.Button exitButton;
    @FXML private javafx.scene.control.Button examButton;
    @FXML private Label title;
    @FXML private Text errorText;
    @FXML private Label nameLabel;
    @FXML private TextField nameField;
    @FXML private Label numberOfQuestionsLabel;
    @FXML private ComboBox<Integer> numberOfQuestions;
    @FXML private Label typeOfQuestionsLabel;
    @FXML private Label typeOfAnswersLabel;
    @FXML private Label questionFullNameLabel;
    @FXML private CheckBox questionFullName;
    @FXML private Label questionOneLetterCodeLabel;
    @FXML private CheckBox questionOneLetterCode;
    @FXML private Label questionThreeLetterCodeLabel;
    @FXML private CheckBox questionThreeLetterCode;
    @FXML private Label questionHydroLabel;
    @FXML private CheckBox questionHydro;
    @FXML private Label questionChargeLabel;
    @FXML private CheckBox questionCharge;
    @FXML private Label questionSizeLabel;
    @FXML private CheckBox questionSize;
    @FXML private Label question3DLabel;
    @FXML private CheckBox question3D;
    @FXML private Label questionStructureLabel;
    @FXML private CheckBox questionStructure;
    @FXML private Label answerFullNameLabel;
    @FXML private CheckBox answerFullName;
    @FXML private Label answerOneLetterCodeLabel;
    @FXML private CheckBox answerOneLetterCode;
    @FXML private Label answerThreeLetterCodeLabel;
    @FXML private CheckBox answerThreeLetterCode;
    @FXML private Label answerHydroLabel;
    @FXML private CheckBox answerHydro;
    @FXML private Label answerChargeLabel;
    @FXML private CheckBox answerCharge;
    @FXML private Label answerSizeLabel;
    @FXML private CheckBox answerSize;
    @FXML private Label answer3DLabel;
    @FXML private CheckBox answer3D;
    @FXML private Label answerStructureLabel;
    @FXML private CheckBox answerStructure;
    @FXML private Label timeOnLabel;
    @FXML private CheckBox timeOn;
    @FXML private Label numberofSecondsLabel;
    @FXML private ComboBox<Integer> numberOfSeconds;
    @FXML private javafx.scene.control.Button discardButton;
    @FXML private javafx.scene.control.Button confirmButton;
    private AudioClip shutDown = new AudioClip(getClass().getResource("music/shutDown.wav").toExternalForm());

    @FXML protected void initialize(){
        labelSetter();
        settingsSetter();
        timeOnListener();
        if (nameField.getCharacters().toString().equals("")){
            setError("Name cannot be empty.");
        } else if (settingsReader.getSettings(false).size() == 0) {
            setError("Please choose at least one category of questioning.");
        } else if (settingsReader.getSettings(true).size() == 0) {
            setError("Please choose at least one category of answers.");
        }
    }

    private void labelSetter(){
        exitButton.setText("Exit");
        examButton.setText("Take exam");
        //Title
        title.setText("Settings");
        //Name
        nameLabel.setText("Name:");
        //Label of number of questions
        numberOfQuestionsLabel.setText("Number of questions:");
        //Fill combobox and set default choice
        numberOfQuestions.getItems().addAll(30, 40, 50, 60, 70);
        typeOfQuestionsLabel.setText("Type of questions to test:");
        //Label setter for questions
        typeOfQuestionSetter(questionFullNameLabel, questionOneLetterCodeLabel, questionThreeLetterCodeLabel,
                questionHydroLabel, questionChargeLabel, questionSizeLabel, question3DLabel,
                questionStructureLabel);
        typeOfAnswersLabel.setText("Type of answers:");
        typeOfQuestionSetter(answerFullNameLabel, answerOneLetterCodeLabel, answerThreeLetterCodeLabel,
                answerHydroLabel, answerChargeLabel, answerSizeLabel, answer3DLabel,
                answerStructureLabel);
        timeOnLabel.setText("Time Enabled?");
        numberofSecondsLabel.setText("Number of seconds");
        numberOfSeconds.getItems().addAll(5, 10, 15, 20);
    }

    private void typeOfQuestionSetter(Label fullNameLabel, Label oneLetterCodeLabel,
                                      Label threeLetterCodeLabel, Label hydroLabel, Label chargeLabel,
                                      Label sizeLabel, Label threeDimensionalLabel, Label structureLabel){
        //Full name label
        fullNameLabel.setText("Full name");
        oneLetterCodeLabel.setText("One letter code");
        threeLetterCodeLabel.setText("Three letter code");
        hydroLabel.setText("Hydrophobic and hydrophilic");
        chargeLabel.setText("Charge");
        sizeLabel.setText("Size");
        threeDimensionalLabel.setText("3D preferation");
        structureLabel.setText("Structure");
    }

    private void settingsSetter() {
        nameField.setText(settingsReader.getName());
        numberOfQuestions.setValue(settingsReader.getNumberOfQuestions());
        questionFullName.setSelected(settingsReader.getFull(false));
        questionOneLetterCode.setSelected(settingsReader.getOne(false));
        questionThreeLetterCode.setSelected(settingsReader.getThree(false));
        questionHydro.setSelected(settingsReader.getHydro(false));
        questionCharge.setSelected(settingsReader.getCharge(false));
        questionSize.setSelected(settingsReader.getSize(false));
        question3D.setSelected(settingsReader.get3D(false));
        questionStructure.setSelected(settingsReader.getStructure(false));
        answerFullName.setSelected(settingsReader.getFull(true));
        answerOneLetterCode.setSelected(settingsReader.getOne(true));
        answerThreeLetterCode.setSelected(settingsReader.getThree(true));
        answerHydro.setSelected(settingsReader.getHydro(true));
        answerCharge.setSelected(settingsReader.getCharge(true));
        answerSize.setSelected(settingsReader.getSize(true));
        answer3D.setSelected(settingsReader.get3D(true));
        answerStructure.setSelected(settingsReader.getStructure(true));
        timeOn.setSelected(settingsReader.getTimeOn());
        numberOfSeconds.setValue(settingsReader.getTime());
        //Enabling or disabling numberOfSeconds
        numberOfSeconds.setDisable(!timeOn.isSelected());
    }

    private void timeOnListener(){
        timeOn.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                numberOfSeconds.setDisable(!newValue);
            }
        });
    }

    @FXML protected void discardButtonAction(){
        //Don't save changes so basically go back
        settingsReader.resetSettings();
        changeScene("homeScreen");
    }

    @FXML protected void confirmButtonAction(){
        checkSettings("homeScreen", false);
    }

    @FXML protected void exitButtonAction(){
        final Task task = new Task() {

            @Override
            protected Object call() {
                shutDown.setVolume(0.5f);
                shutDown.setCycleCount(INDEFINITE);
                shutDown.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        try {
            Thread.sleep(1750);}
        catch (InterruptedException ie){
            System.out.println("Couldn´t sleep...");
        }
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();

    }

    @FXML protected void examButtonAction(){
        //Go to exam screen
        checkSettings("playScreen", true);
    }

    @FXML private void changeScene(String scene) {
        try {
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) title.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length()-6) + " screen");
        } catch (IOException IOE){
            System.out.println("Whoops, couldn't load the new scene...");
        }
    }

    private void checkSettings(String whereTo, Boolean fromOptionsScreen){
        //Save settings in settings.txt and go to whereTo
        settingsReader.saveSettings(nameField.getCharacters().toString(), numberOfQuestions.getValue(),
                questionFullName.isSelected(), questionOneLetterCode.isSelected(), questionThreeLetterCode.isSelected(),
                questionHydro.isSelected(), questionCharge.isSelected(), questionSize.isSelected(),
                question3D.isSelected(), questionStructure.isSelected(), answerFullName.isSelected(),
                answerOneLetterCode.isSelected(), answerThreeLetterCode.isSelected(), answerHydro.isSelected(),
                answerCharge.isSelected(), answerSize.isSelected(), answer3D.isSelected(),
                answerStructure.isSelected(), timeOn.isSelected(), numberOfSeconds.getValue(), fromOptionsScreen);
        if (nameField.getCharacters().toString().equals("") || nameField.getCharacters().toString().equals(" ")){
            setError("Name cannot be empty.");
        } else if (settingsReader.getSettings(false).size() < 1) {
            setError("Please choose at least one category of questioning.");
        } else if (settingsReader.getSettings(true).size() < 1) {
            setError("Please choose at least one category of answers.");
        } else if (settingsReader.getSettings(false).equals(settingsReader.getSettings(true)) && settingsReader.getSettings(false).size() == 1){
            setError("Please select different questions than answers.");
        } else if (!settingsReader.topThree(false) && !settingsReader.topThree(true)){
            setError("Please select at least one of these 6 categories");
            colorTopThree();
        } else {
            changeScene(whereTo);
        }
    }

    private void colorTopThree(){
        Scene sceneTheLabelBelongs = title.getScene();
        sceneTheLabelBelongs.getStylesheets().add("ElsMoo/coloringCheckBoxes.css");
        questionFullName.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
        questionOneLetterCode.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
        questionThreeLetterCode.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
        answerFullName.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
        answerOneLetterCode.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
        answerThreeLetterCode.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                removeOrAdd(newValue);
            }
        });
    }

    //Not beauty but it works
    private void removeOrAdd(Boolean rOrA){
        if (questionFullName.isSelected() || questionOneLetterCode.isSelected() || questionThreeLetterCode.isSelected() || answerFullName.isSelected() || answerOneLetterCode.isSelected() || answerThreeLetterCode.isSelected()){
            Scene sceneTheLabelBelongs = title.getScene();
            sceneTheLabelBelongs.getStylesheets().remove("ElsMoo/coloringCheckBoxes.css");
        } else {
            Scene sceneTheLabelBelongs = title.getScene();
            sceneTheLabelBelongs.getStylesheets().add("ElsMoo/coloringCheckBoxes.css");
        }
    }

    private void setError(String error){
        errorText.setText(error);
    }
}
