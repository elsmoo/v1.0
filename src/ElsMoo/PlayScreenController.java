package ElsMoo;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.ArrayList;

import static javafx.scene.media.MediaPlayer.INDEFINITE;

public class PlayScreenController {
    @FXML
    private Text name;
    @FXML
    private Text question;
    @FXML
    private ImageView sidechainQuestion = new ImageView();
    @FXML
    private javafx.scene.control.Button answer1;
    @FXML
    private javafx.scene.control.Button answer2;
    @FXML
    private javafx.scene.control.Button answer3;
    @FXML
    private javafx.scene.control.Button answer4 = new Button("The Fourth answer");
    @FXML
    private javafx.scene.control.Button goodLuckButton;
    @FXML
    private javafx.scene.control.Button nextButton;
    @FXML
    private javafx.scene.control.Button backButton;
    @FXML
    private javafx.scene.control.Button exitButton;
    @FXML
    private javafx.scene.layout.GridPane gridPane;
    @FXML
    private ProgressBar timeBalk = new ProgressBar(0);
    @FXML
    private HBox hBox;
    @FXML
    private Timeline taskTimeBalk;
    @FXML
    private AudioClip audio = new AudioClip(getClass().getResource("music/" + settingsReader.getTime() + "Sec.wav").toExternalForm());
    private AudioClip shutDown = new AudioClip(getClass().getResource("music/shutDown.wav").toExternalForm());
    private int maxQuestion = settingsReader.getNumberOfQuestions();
    private ArrayList<String> currentQuestion;
    private DatabaseConnecter databaseConnecter = new DatabaseConnecter();
    private byte questionNumber = 0;
    private int countCorrectAnswer = 0;
    private int countWrongAnswer = 0;
    private int countNoAnswer = 0;

    @FXML
    protected void goodLuckButtonAction() {
        name.setText(settingsReader.getName() + ",");
        goodLuckButton.setVisible(false);
        if (settingsReader.getTimeOn()) {
            timeBalk.setPrefWidth(250);
            hBox.getChildren().add(timeBalk);
            taskTimeBalk = new Timeline(
                    new KeyFrame(
                            Duration.ZERO,
                            new KeyValue(timeBalk.progressProperty(), 0)
                    ),
                    new KeyFrame(
                            Duration.seconds(settingsReader.getTime()),
                            new KeyValue(timeBalk.progressProperty(), 1)
                    )
            );
            taskTimeBalk.playFromStart();
            taskTimeBalk.setOnFinished(restart -> timeIsUp());
        }
        askQuestion();
    }

    private void timeIsUp() {
        countNoAnswer++;
        returnToNormal();
        askQuestion();
    }

    private void askQuestion() {
        if (settingsReader.getTimeOn()) {
            audio.stop();
            audio.play();
            taskTimeBalk.playFromStart();
        }
        if (questionNumber < maxQuestion) {
//            System.out.println("Questions Left: " + (maxQuestion - questionNumber) + "\nGood answers: " + countCorrectAnswer + "\nWrong Answers: " + countWrongAnswer + "\nNo Answers: " + countNoAnswer);
            currentQuestion = new ArrayList<String>();
            currentQuestion.addAll(databaseConnecter.getQuestion(settingsReader.getSettings(false), settingsReader.getSettings(true)));
            question.setText(currentQuestion.get(0));
            answer1.setText(currentQuestion.get(2));
            answer2.setText(currentQuestion.get(3));
            answer3.setText(currentQuestion.get(4));
            if (currentQuestion.size() == 6) {
                gridPane.add(answer4, 0, 6);
                gridPane.setColumnSpan(answer4, 2);
                gridPane.setHalignment(answer4, HPos.CENTER);
                answer4.setText(currentQuestion.get(5));
            } else if (currentQuestion.get(0).contains("sidechain") && currentQuestion.get(0).contains("following")) {
                sidechainQuestion.setImage(new Image(getClass().getResource("images/" + currentQuestion.get(0).toUpperCase().charAt(currentQuestion.get(0).length() - 2) + ".png").toExternalForm()));
                question.setText(currentQuestion.get(0).substring(0, currentQuestion.get(0).length() - 2));
                gridPane.add(sidechainQuestion, 0, 2);
                gridPane.setHalignment(sidechainQuestion, HPos.CENTER);
                gridPane.setColumnSpan(sidechainQuestion, 2);
                gridPane.setColumnSpan(sidechainQuestion, 2);
            } else if (currentQuestion.get(0).contains("sidechain")) {
                answer1.setText("");
                answer2.setText("");
                answer3.setText("");
                answer1.setGraphic(new ImageView(new Image(getClass().getResource("images/" + currentQuestion.get(2).toUpperCase() + ".png").toExternalForm())));
                answer2.setGraphic(new ImageView(new Image(getClass().getResource("images/" + currentQuestion.get(3).toUpperCase() + ".png").toExternalForm())));
                answer3.setGraphic(new ImageView(new Image(getClass().getResource("images/" + currentQuestion.get(4).toUpperCase() + ".png").toExternalForm())));
            } else {
                returnToNormal();
            }
            questionNumber++;
            if ((maxQuestion - questionNumber) == 0) {
                nextButton.setText("Results");
            }
        } else {
            if (settingsReader.getTimeOn()) {
                taskTimeBalk.stop();
            }
            goToResults();
        }
    }

    @FXML
    protected void answerButtonAction(ActionEvent actionEvent) {
        //Check answer, Count them, and ask new question
        String answer = ((Button) actionEvent.getSource()).getText();
        if (answer.equals(currentQuestion.get(1))) {
            countCorrectAnswer++;
        } else {
            countWrongAnswer++;
        }
        returnToNormal();
        askQuestion();
    }

    @FXML
    protected void nextButtonAction() {
        countNoAnswer++;
        returnToNormal();
        askQuestion();
    }

    @FXML
    protected void backButtonAction() {
        if (settingsReader.getTimeOn()) {
            taskTimeBalk.stop();
        }
        if (settingsReader.getFromOptionsScreen()) {
            changeScene("optionsScreen");
        } else {
            changeScene("homeScreen");

        }
    }

    @FXML
    protected void exitButtonAction() {
        audio.stop();
        final Task task = new Task() {

            @Override
            protected Object call() {
                shutDown.setVolume(0.5f);
                shutDown.setCycleCount(INDEFINITE);
                shutDown.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        try {
            Thread.sleep(1750);
        } catch (InterruptedException ie) {
            System.out.println("Couldn´t sleep...");
        }
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    private void returnToNormal() {
        gridPane.getChildren().remove(answer4);
        gridPane.getChildren().remove(sidechainQuestion);
        answer1.setGraphic(null);
        answer2.setGraphic(null);
        answer3.setGraphic(null);
    }

    private void changeScene(String scene) {
        try {
            audio.stop();
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) question.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length()-6) + " screen");
        } catch (IOException IOE) {
            System.out.println("Whoops, couldn't load the new scene...");
        }
    }

    private void goToResults() {
        audio.stop();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("resultScreen.fxml"));
            ResultScreenController resultScreenController = new ResultScreenController(countCorrectAnswer);
            loader.setController(resultScreenController);
            Scene homeScreen = new Scene(loader.load(), 500, 600);
            Stage stageTheLabelBelongs = (Stage) question.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle("Result screen");
        } catch (IOException IOE) {
            System.out.println("Whoops");
        }
    }
}

