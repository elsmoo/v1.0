package ElsMoo;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

import static javafx.scene.media.MediaPlayer.INDEFINITE;

public class ResultScreenController {
    @FXML private Text name;
    @FXML private VBox result;
    @FXML private javafx.scene.control.Button backButton;
    @FXML private javafx.scene.control.Button exitButton;
    private AudioClip shutDown = new AudioClip(getClass().getResource("music/shutDown.wav").toExternalForm());
    private int correct = 69;

    ResultScreenController(int correct){
        this.correct = correct;
    }

    @FXML protected void initialize() {
        name.setText(settingsReader.getName() + name.getText());
        Button showResults = new Button("Show results");
        showResults.setOnAction(e -> onButton());
        result.getChildren().add(showResults);
    }

    @FXML private void onButton(){
        result.getChildren().clear();
        result.getChildren().add(new Fraction(correct, settingsReader.getNumberOfQuestions()));
        if (this.correct/settingsReader.getNumberOfQuestions() > 5.5) {
            Scene sceneTheLabelBelongs = name.getScene();
            sceneTheLabelBelongs.getStylesheets().remove("ElsMoo/pass.css");
        } else {
            Scene sceneTheLabelBelongs = name.getScene();
            sceneTheLabelBelongs.getStylesheets().add("ElsMoo/fail.css");
        }
    }

    @FXML
    protected void backButtonAction() {
        changeScene("homeScreen");
    }

    @FXML
    protected void exitButtonAction(){
        final Task task = new Task() {

            @Override
            protected Object call() {
                shutDown.setVolume(0.5f);
                shutDown.setCycleCount(INDEFINITE);
                shutDown.play();
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        try {
        Thread.sleep(1750);}
        catch (InterruptedException ie){
            System.out.println("Couldn´t sleep...");
        }
        // get a handle to the stage
        Stage stage = (Stage) exitButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    @FXML private void changeScene(String scene) {
        try {
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) exitButton.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length()-6) + " screen");
        } catch (IOException IOE){
            System.out.println("Whoops, couldn't load the new scene...");
        }
    }
}
