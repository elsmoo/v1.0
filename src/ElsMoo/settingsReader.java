package ElsMoo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

class settingsReader {

    /**
     * Returns username
     *
     * @return String, if the username == " " then it returns an empty string. Otherwise the current username will be
     * returned.
     */
    static String getName() {
        if (openSettings(0).equals(" ")) {
            return "";
        } else {
            return openSettings(0);
        }
    }

    static Integer getNumberOfQuestions() {
        return Integer.parseInt(openSettings(1));
    }

    static Boolean getFull(Boolean qOrA) {
        return Boolean.valueOf(openSettings(2 + (8 * ternary(qOrA))));
    }

    static Boolean getOne(Boolean qOrA) {
        return Boolean.valueOf(openSettings(3 + (8 * ternary(qOrA))));
    }

    static Boolean getThree(Boolean qOrA) {
        return Boolean.valueOf(openSettings(4 + (8 * ternary(qOrA))));
    }

    static Boolean getHydro(Boolean qOrA) {
        return Boolean.valueOf(openSettings(5 + (8 * ternary(qOrA))));
    }

    static Boolean getCharge(Boolean qOrA) {
        return Boolean.valueOf(openSettings(6 + (8 * ternary(qOrA))));
    }

    static Boolean getSize(Boolean qOrA) {
        return Boolean.valueOf(openSettings(7 + (8 * ternary(qOrA))));
    }

    static Boolean get3D(Boolean qOrA) {
        return Boolean.valueOf(openSettings(8 + (8 * ternary(qOrA))));
    }

    static Boolean getStructure(Boolean qOrA) {
        return Boolean.valueOf(openSettings(9 + (8 * ternary(qOrA))));
    }

    static Boolean getTimeOn() {
        return Boolean.valueOf(openSettings(18));
    }

    static Integer getTime() {
        return Integer.parseInt(openSettings(19));
    }

    static Boolean getFromOptionsScreen() {
        return Boolean.valueOf(openSettings(20));
    }

    static ArrayList<String> getSettings(Boolean qOrA) {
        ArrayList<String> typeOfQuestions = new ArrayList<>();
        for (int i = 9 * ternary(qOrA); i <= 8 + (8 * ternary(qOrA)); i++) {
            if (Boolean.valueOf(openSettings(i + 1))) {
                typeOfQuestions.add(openSettings(i + 1, 0).substring((openSettings(i + 1, 0).toLowerCase().charAt(0) == 'q') ? 8 : 6).toLowerCase());
            }
        }
        return typeOfQuestions;
    }

    static Boolean topThree(Boolean qOrA) {
        for (int i = 9 * ternary(qOrA); i <= 3 + (8 * ternary(qOrA)); i++) {
            if (Boolean.valueOf(openSettings(i + 1))) {
                return true;
            }
        }
        return false;
    }

    private static int ternary(Boolean value) {
        return value ? 1 : 0;
    }

    /**
     * Opens the settings.txt file and gives the setting at the given line
     *
     * @param line Integer, line (and setting) to return
     * @return String, the setting from the given line
     */
    private static String openSettings(int line) {
        try {
            return Files.readAllLines(Paths.get("settings.txt")).get(line).split("=")[1];
        } catch (IOException IOE) {
            return "";
        }
    }

    private static String openSettings(int line, int afterSplit) {
        try {
            return Files.readAllLines(Paths.get("settings.txt")).get(line).split("=")[afterSplit];
        } catch (IOException IOE) {
            return "";
        }
    }

    /**
     * Saves the settings in the settings.txt file.
     *
     * @param name                     String of the username
     * @param numberOfQuestions        Integer, the amount of questions in the test
     * @param questionFull             Boolean, setting for the full name of the aminoacids is going to be tested
     * @param questionOne              Boolean, setting for the questionOne letter code of the aminoacids is going to be tested
     * @param questionThree            Boolean, setting for the questionThree letter code of the aminoacids is going to be tested
     * @param questionHydro            Boolean, setting for the hydrophic properties of the aminoacids is going to be tested
     * @param questionCharge           Boolean, setting for the questionCharge of the aminoacids is going to be tested
     * @param questionSize             Boolean, setting for the questionSize of the aminoacids is going to be tested
     * @param questionThreeDimensional Boolean, setting for the 3-Dimensional questionStructure of the aminoacids is going to be tested
     * @param questionStructure        Boolean, setting for the (2D) questionStructure of the aminoacids is going to be tested
     * @param timeOn                   Boolean, setting for a timer per question
     * @param numberOfSeconds          Integer, amount of seconds per question (if timeOn == 1)
     */
    static void saveSettings(String name, Integer numberOfQuestions, Boolean questionFull, Boolean questionOne,
                             Boolean questionThree, Boolean questionHydro, Boolean questionCharge, Boolean questionSize,
                             Boolean questionThreeDimensional, Boolean questionStructure, Boolean answerFull,
                             Boolean answerOne, Boolean answerThree, Boolean answerHydro, Boolean answerCharge,
                             Boolean answerSize, Boolean answerThreeDimensional, Boolean answerStructure,
                             Boolean timeOn, Integer numberOfSeconds, Boolean fromOptionsScreen) {
        try {
            PrintWriter writer = new PrintWriter("settings.txt", "UTF-8");
            if (name.equals("")) {
                writer.println("name= ");
            } else {
                writer.println("name=" + name);
            }
            writer.println("numberOfQuestions=" + numberOfQuestions);
            writer.println("questionFullName=" + questionFull);
            writer.println("questionOneLetterCode=" + questionOne);
            writer.println("questionThreeLetterCode=" + questionThree);
            writer.println("questionHydro=" + questionHydro);
            writer.println("questionCharge=" + questionCharge);
            writer.println("questionSize=" + questionSize);
            writer.println("questionpref=" + questionThreeDimensional);
            writer.println("questionStructure=" + questionStructure);
            writer.println("answerFullName=" + answerFull);
            writer.println("answerOneLetterCode=" + answerOne);
            writer.println("answerThreeLetterCode=" + answerThree);
            writer.println("answerHydro=" + answerHydro);
            writer.println("answerCharge=" + answerCharge);
            writer.println("answerSize=" + answerSize);
            writer.println("answerpref=" + answerThreeDimensional);
            writer.println("answerStructure=" + answerStructure);
            writer.println("timeOn=" + timeOn);
            writer.println("time=" + numberOfSeconds);
            writer.println("fromOptionsScreen=" + fromOptionsScreen);
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException settingsWriterError) {
            System.out.println("Error writing settings, returning to previous settings");
        }
    }

    static void resetSettings() {
        try {
            PrintWriter writer = new PrintWriter("settings.txt", "UTF-8");
            writer.println("name= ");
            writer.println("numberOfQuestions=50");
            writer.println("questionFullName=false");
            writer.println("questionOneLetterCode=false");
            writer.println("questionThreeLetterCode=false");
            writer.println("questionHydro=false");
            writer.println("questionCharge=false");
            writer.println("questionSize=false");
            writer.println("questionpref=false");
            writer.println("questionStructure=false");
            writer.println("answerFullName=false");
            writer.println("answerOneLetterCode=false");
            writer.println("answerThreeLetterCode=false");
            writer.println("answerHydro=false");
            writer.println("answerCharge=false");
            writer.println("answerSize=false");
            writer.println("answerpref=false");
            writer.println("answerStructure=false");
            writer.println("timeOn=false");
            writer.println("time=10");
            writer.println("fromOptionsScreen=false");
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException settingsWriterError) {
            System.out.println("Error writing settings, returning to previous settings");
        }

    }
}
