package homeWerkJobbird;

import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Afsluiten extends Application {

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btAfsluiten = new Button("Afsluiten");
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(btAfsluiten);

        primaryStage.show();
    }
}
