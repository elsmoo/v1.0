package homeWerkJobbird;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Scherm2 extends Application {
    Button btn = new Button("Naar scherm 1");

    @Override
    public void start(Stage primaryStage) {
        Label lbl = new Label("Dit is scherm 2.");
        VBox vb = new VBox(20, lbl, this.btn);

        Scene scene = new Scene(vb, 500, 500);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
