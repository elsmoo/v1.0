package homeWerkJobbird;

import javafx.application.Application;
import javafx.stage.Stage;

public class SchermMain extends Application {
    Scherm1 scherm1 = new Scherm1();
    Scherm2 scherm2 = new Scherm2();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        scherm1.start(primaryStage);

        scherm1.btn.setOnAction(p -> {
            scherm2.start(primaryStage);
        });

        scherm2.btn.setOnAction(p -> {
            scherm1.start(primaryStage);
        });
    }
}
